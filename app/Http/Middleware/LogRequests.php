<?php

namespace App\Http\Middleware;

use App\RequestLog;
use Closure;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $log = new RequestLog();
        $log->ip     = $request->ip;
        $log->url    = $request->url();
        $log->method = $request->method();
        $log->save();
        return $next($request);
    }
}

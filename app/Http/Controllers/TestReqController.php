<?php

namespace App\Http\Controllers;

use App\RequestLog;
use Illuminate\Http\Request;

class TestReqController extends Controller
{
    public function index(){
        return RequestLog::groupBy('ip')
            ->select(['ip',\DB::raw('count(ip) as Visits')])
            ->orderBy('Visits','DESC')->take(10)->get();
    }

    public function store(){

    }

    public function update(){

    }

    public function destroy(){

    }
}
